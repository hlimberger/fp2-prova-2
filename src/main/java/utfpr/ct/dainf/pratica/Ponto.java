package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    
    // Construtores Publicos
    public Ponto(){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    public Ponto(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    // Getters e Setters
    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }
    
        public void setZ(double z){
        this.z = z;
    }
        
    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }
    
    public double getZ(){
        return this.z;
    }
    
    // Nome da classe
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    // String da classe
    @Override
    public String toString(){
        return this.getNome() + "(" + this.getX() + "," + this.getY() + "," + this.getZ() + ")";
    }
    
    // Comparar pontos
    @Override
    public boolean equals(Object obj){
        if (obj instanceof Ponto){
            Ponto ponto = (Ponto)obj;
            if(ponto.x == this.x && ponto.y == this.y && ponto.z == this.z){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    // Medir distancia
    public double dist(Ponto ponto){
        return Math.sqrt(Math.pow(ponto.x-this.x,2)+Math.pow(ponto.y-this.y,2)+Math.pow(ponto.z-this.z,2));
    }
    
    
    
}
