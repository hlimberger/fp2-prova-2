/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author henrique
 */
public class PontoXZ extends Ponto2D{
    
    public PontoXZ(){
        super();
    }
    
    public PontoXZ(double x, double z){
        super(x,0,z);
    }
    
    // String da classe
    @Override
    public String toString(){
        return this.getNome() + "(" + this.getX() + "," + this.getZ() + ")";
    }
}
