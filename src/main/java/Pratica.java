/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
import utfpr.ct.dainf.pratica.*;

public class Pratica {

    public static void main(String[] args) {
        PontoXZ p1 = new PontoXZ(-3,2);
        PontoXY p2 = new PontoXY(0,2);
        System.out.println("Distancia = " + p1.dist(p2));
        
        Ponto p3 = new Ponto(1,2,3);
        Ponto p4 = new Ponto(1,2,3);
        
        boolean comparador = p3.equals(p4);
        System.out.println(p3.toString() + " vs " + p4.toString() + ": " + comparador);
        
        comparador = p1.equals(null);
        System.out.println(p1.toString() + " vs " + null + ": " + comparador);

    }
    
}
